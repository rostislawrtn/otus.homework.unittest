﻿using AutoFixture.AutoMoq;
using AutoFixture;

using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly DateTime _endDate = DateTime.Now.AddDays(60);

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        private Partner CreateDefaultPertner(Guid partnerId)
        {
            return new Partner() 
            {
                Id = partnerId,
                IsActive = true,
                Name = "Test",
                NumberIssuedPromoCodes = 1
            };
        }

        private PartnerPromoCodeLimit CreateDefaultPartnerPromoCodeLimit(Guid partnerId)
        {
            return new PartnerPromoCodeLimit() 
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100,
                PartnerId = partnerId
            };
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = 1
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = this.CreateDefaultPertner(partnerId);
            partner.IsActive = false;

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = 1
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

            var objectResult = result as ObjectResult;
            string actual = objectResult.Value.ToString();
            string expected = "Данный партнер не активен";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNegativeLimit_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            ICollection<PartnerPromoCodeLimit> partnerLimits = new List<PartnerPromoCodeLimit>()
            {
                this.CreateDefaultPartnerPromoCodeLimit(partnerId)
            };

            Partner partner = this.CreateDefaultPertner(partnerId);
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = -1
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

            var objectResult = result as ObjectResult;
            string actual = objectResult.Value.ToString();
            string expected = "Лимит должен быть больше 0";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_ReturnsCorrect()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

            ICollection<PartnerPromoCodeLimit> partnerLimits = new List<PartnerPromoCodeLimit>()
            {
                this.CreateDefaultPartnerPromoCodeLimit(partnerId)
            };

            Partner partner = this.CreateDefaultPertner(partnerId);
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = 123
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            var objectResponse = ((result as ObjectResult).Value as ObjectResult).Value as PartnerPromoCodeLimitResponse;
            Assert.Equal(objectResponse.Limit, 123);
            Assert.Equal(objectResponse.PartnerId, partnerId);
            Assert.Equal(objectResponse.EndDate, this._endDate.ToString("dd.MM.yyyy hh:mm:ss"));
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_LastLemitIsCancel()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

            List<PartnerPromoCodeLimit> partnerLimits = new List<PartnerPromoCodeLimit>()
            {
                this.CreateDefaultPartnerPromoCodeLimit(partnerId)
            };

            var lastLimit = partnerLimits.First();

            Partner partner = this.CreateDefaultPertner(partnerId);
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = 123
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            Assert.NotNull(lastLimit.CancelDate);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_NumberIssuedPromoCodesMustBeClear()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

            List<PartnerPromoCodeLimit> partnerLimits = new List<PartnerPromoCodeLimit>()
            {
                this.CreateDefaultPartnerPromoCodeLimit(partnerId)
            };

            Partner partner = this.CreateDefaultPertner(partnerId);
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var parameter = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = this._endDate,
                Limit = 123
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, parameter);

            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }

    }
}